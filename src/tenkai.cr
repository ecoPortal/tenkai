require "clim"
require "colorize"
require "json"
require "./tenkai/failed_command"
require "./tenkai/**"

module Tenkai
  @@checked_credentials = false
  def self.cmd(base, args = [] of String, shell = false)
    check_credentials if base == "aws"
    debug_out do
      "(exec): #{base} #{args.join(" ")}"
    end
    io     = IO::Memory.new
    err    = IO::Memory.new
    status = Process.run(base, args: args, shell: shell, output: io, error: err)
    io.close
    err.close

    unless status.success?
      raise FailedCommand.new(
        base, args, status.exit_status, err.to_s
      )
    end
    return io.to_s
  end
  def self.shell(command : String)
    args = ["-c", command]
    cmd("bash", args, true)
  end
  def self.check_credentials
    return if @@checked_credentials
    @@checked_credentials = true
    # Check the user has AWS credentials correctly configured
    begin
      Tenkai.cmd("aws", ["sts", "get-caller-identity"])
    rescue e : Tenkai::FailedCommand
      puts "Not currently signed in to AWS. Please run:"
      puts "  aws configure"
      exit(1)
    end
  end

  @@whoami : String?
  def self.whoami
    @@whoami ||= Tenkai.cmd("whoami").chomp
  end
  def self.debug_out(&block : -> String)
    if ENV.has_key?("TENKAI_DEBUG") && ENV["TENKAI_DEBUG"] == "1"
      value = yield
      puts value
    end
  end
  def self.run!
    # Enable safe colouring of command output
    Colorize.on_tty_only!
    # Enable aliases
    if ARGV.size > 0 && Tenkai.config.has_alias?(ARGV[0])
      args = Tenkai.config.get_alias(ARGV[0])
      args = args.concat(ARGV[1..-1]) if ARGV.size > 1
    else
      args = ARGV
    end
    # Enable alias rewriting - can't do it with CLIM as it will
    # eat the options
    if args.size > 0
      case args.first
      when "alias"
        Tenkai.config.add_alias(args[1..-1])
        exit
      when "unalias"
        Tenkai.config.remove_alias(args[1..-1])
        exit
      when "aliases"
        Tenkai.config.list_aliases
        exit
      end
    end
    # Launch the CLI
    begin
      Tenkai::CLI.start(args)
    rescue e : Tenkai::FailedCommand
      puts e
      exit e.status
    end
  end
  def self.config
    @@config ||= Tenkai::Config.new
  end
end

# Entrypoint
Tenkai.run!
