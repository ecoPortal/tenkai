require "json"

module Tenkai
  class Console
    def initialize(cluster : String, task_definition : String, certificate : String?, region : String, cmd : String, copy_path : String?)
      @cluster         = cluster
      @task_definition = task_definition
      @certificate     = certificate
      @region          = region
      @cmd             = cmd
      @stopped         = false
      @copy_path       = copy_path
    end

    def run
      arns = launch_task()
      at_exit { stop_task(arns[:task]) }
      begin
        ip = get_instance_ip(arns[:instance])
        docker_id = poll_for_docker_id(ip, arns[:task])
	      unless @copy_path.nil?
          copy_files_to_docker(ip, docker_id, @copy_path.to_s)
	      end
        exec_docker(ip, docker_id)
      ensure
        stop_task arns[:task]
      end
    end

    def exec_docker(ip, docker_id)
      exec_ssh(ip, "docker exec -ti \"#{docker_id}\" #{@cmd}")
    end

    def exec_ssh(ip, command)
      args  = [
        "-qt", "ec2-user@#{ip}",
        "-o", "StrictHostKeyChecking=no",
        command,
      ]
      unless @certificate.nil?
        args.unshift("-i", @certificate.to_s)
      end

      process = Process.fork do
        Process.exec("ssh", args: args)
      end
      process.wait
    end

    def copy_files_to_docker(ip, docker_id, copy_path)
      print "-> Copying files..\n"
      exec_ssh(ip, "mkdir -p /tmp/upload")
      exec_scp(ip, copy_path, "/tmp/upload/")
      exec_ssh(ip, "docker cp /tmp/upload #{docker_id}:/app")
      exec_ssh(ip, "rm -rf /tmp/upload/*")
    end

    def exec_scp(ip, from, to)
      args = ["-r", from, "ec2-user@#{ip}:#{to}"]
      unless @certificate.nil?
        args.unshift("-i", @certificate.to_s)
      end
      Tenkai.shell("scp " + args.join(" "))
   end

    def poll_for_docker_id(ip, task_arn)
      STDOUT.sync = true if STDOUT.tty?
      attempt = 0
      print "-> Waiting for container to start.."
      while attempt < 50
        print "."

        id = get_docker_id(ip, task_arn)
        if id
          puts
          return id
        end

        attempt += 1
        sleep 4
      end
      puts
      puts "-> Container didn't start in time"
      exit 1
    end

    def get_docker_id(ip, task_arn)
      args  = [
        "-qt", "ec2-user@#{ip}",
        "-o", "StrictHostKeyChecking=no",
        "curl http://localhost:51678/v1/tasks?taskarn=#{task_arn}"
      ]
      unless @certificate.nil?
        args.unshift("-i", @certificate.to_s)
      end
      response = Tenkai.cmd("ssh", args)
      begin
        JSON.parse(response)["Containers"][0]["DockerId"].as_s
      rescue KeyError
      rescue JSON::ParseException
      rescue Exception
      end
    end

    def get_instance_ip(instance_arn : String)
      container_instances = JSON.parse Tenkai.cmd("aws", [
        "ecs", "describe-container-instances",
        "--region", @region,
        "--cluster", @cluster,
        "--container-instances", instance_arn
      ])
      instance_id = container_instances["containerInstances"][0]["ec2InstanceId"].as_s
      instance = JSON.parse Tenkai.cmd("aws", [
        "ec2", "describe-instances",
        "--region", @region,
        "--instance-ids", instance_id
      ])

      return instance["Reservations"][0]["Instances"][0]["PublicIpAddress"].as_s
    end

    def timestamp
      Time.local.to_s("%FT%R")
    end

    def launch_task
      puts "-> Starting task..."
      response = Tenkai.cmd("aws", [
        "ecs", "run-task",
        "--region", @region,
        "--cluster", @cluster,
        "--task-definition", @task_definition,
        "--overrides", overrides_json,
        # We limit the started by stamp to 36 characters as that is what AWS
        # supports. A timestamp is included to make it easier to kill zombies
        # without killing your active console session.
        "--started-by", "console/#{timestamp}/#{Tenkai.whoami}"[0,36]
      ])
      parsed_response = JSON.parse(response)
      begin
        task_arn        = parsed_response["tasks"][0]["taskArn"].as_s
        instance_arn    = parsed_response["tasks"][0]["containerInstanceArn"].as_s

        puts "-> Launched task"

        return {
          task:     task_arn,
          instance: instance_arn
        }
      rescue ex : KeyError | IndexError
        puts "Error: could not start task."
        puts response
        exit(1)
      end
    end

    def stop_task(arn : String)
      return if @stopped
      puts "-> Stopping task"
      output = Tenkai.cmd("aws", [
        "ecs", "stop-task",
        "--region", @region,
        "--cluster", @cluster,
        "--task", arn,
        "--reason", "tenkai/stop"
      ])
      @stopped = true
    end

    def console_container_name
      response = Tenkai.cmd("aws", [
        "ecs", "describe-task-definition",
        "--task-definition", @task_definition,
        "--region", @region
      ])
      JSON.parse(response)["taskDefinition"]["containerDefinitions"][0]["name"].as_s
    end

    def overrides_json
      JSON.build do |json|
        json.object do
          json.field "containerOverrides" do
            json.array do
              json.object do
                json.field "name", console_container_name
                json.field "command" do
                  json.array do
                    json.string "sleep"
                    # One day timeout
                    json.string "1d"
                  end
                end
              end
            end
          end
        end
      end
    end
  end
end
