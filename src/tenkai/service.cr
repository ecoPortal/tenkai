module Tenkai
  class Service
    def initialize(cluster : String, region  : String)
      @cluster = cluster
      @region  = region
    end
    def all_services
      response = Tenkai.cmd("aws", [
        "ecs", "list-services",
        "--cluster", @cluster,
        "--region", @region
      ])
      JSON.parse(response)["serviceArns"].as_a.map(&.as_s)
    end
    def get_service_info(arn : String)
      response = Tenkai.cmd("aws", [
        "ecs", "describe-services",
        "--cluster", @cluster,
        "--region", @region,
        "--services", arn
      ])
      JSON.parse(response)["services"].as_a.first.as_h
    end
    @asgs : Hash(String, JSON::Any)?
    def all_asgs
      @asgs ||= begin
                  response = Tenkai.cmd("aws", [
                    "application-autoscaling", "describe-scalable-targets",
                    "--service-namespace", "ecs",
                    "--region", @region
                  ])
                  JSON.parse(response)["ScalableTargets"].as_a.select do |target|
                    target["ScalableDimension"].as_s == "ecs:service:DesiredCount" &&
                      target["ResourceId"].as_s =~ /^service\/#{@cluster}\//
                  end.index_by(&.["ResourceId"].to_s)
                end
    end
    def scale(desired_concurrency : Hash(String, Tuple(UInt8,UInt8)))
      all_services.each do |service|
        key = desired_concurrency.keys.find do |key|
          service.includes?(key)
        end
        if key
          scale_service(service, desired_concurrency[key])
        end
      end
    end
    def scale_service(arn : String, concurrency : Tuple(UInt8, UInt8))
      name     = arn.split("/").last
      min, max = concurrency
      concurrency_as_s = if min == max
                           min.to_s
                         else
                           "#{min},#{max}"
                         end
      puts "-> Scaling #{name}=#{concurrency_as_s}"
      if asg = find_asg(name)
        puts "  -> Autoscaling detected (min=#{asg["MinCapacity"]}, max=#{asg["MaxCapacity"]})"
        update_asg(asg, min, max)
      else
        if min != max
          puts "  -> No autoscaling configured! Can't set min/max"
          exit(1)
        end
        update_service_concurrency(arn, min)
      end
    end
    def update_service_concurrency(arn : String, desired : UInt8)
      puts "  -> Updating desired count to #{desired}"
      Tenkai.cmd("aws", [
        "ecs", "update-service",
        "--cluster", @cluster,
        "--region", @region,
        "--service", arn,
        "--desired-count", desired.to_s
      ])
    end
    def update_asg(asg, min, max)
      puts "  -> Updating scalable target to min=#{min} max=#{max}"
      Tenkai.cmd("aws", [
        "application-autoscaling", "register-scalable-target",
        "--service-namespace", "ecs",
        "--region", @region,
        "--resource-id", asg["ResourceId"].as_s,
        "--scalable-dimension", asg["ScalableDimension"].as_s,
        "--min-capacity", min.to_s,
        "--max-capacity", max.to_s
      ])
    end
    def find_asg(service_name : String)
      key = ["service", @cluster, service_name].join("/")
      all_asgs[key]?
    end
  end
end
