module Tenkai
  class FailedCommand < Exception
    getter status : Int32
    getter stderr : String
    def initialize(command : String, args : Array(String), status : Int32, stderr : String)
      @command = command
      @args    = args
      @status  = status
      @stderr  = stderr
    end
    def message
      "(Failed) #{@command} #{@args.join(" ")}\n"+@stderr
    end
  end
end
