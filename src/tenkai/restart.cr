module Tenkai
  class Restart
    def initialize(cluster : String, region  : String, filters : Array(String) = [] of String)
      @cluster = cluster
      @filters = filters
      @region  = region
    end
    def matching_services
      all_services.select do |service|
        @filters.empty? || @filters.any? do |filter|
          matches?(filter, service)
        end
      end
    end
    def matches?(filter : String, str : String)
      str.includes?(filter)
    end
    def all_services
      response = Tenkai.cmd("aws", [
        "ecs", "list-services",
        "--cluster", @cluster,
        "--region", @region
      ])
      JSON.parse(response)["serviceArns"].as_a.map do |arn|
        arn.as_s.split("/").last
      end
    end
    def run(services = matching_services)
      services.each do |service|
        puts "-> Restarting #{@cluster}/#{service}"
        Tenkai.cmd("aws", [
          "ecs", "update-service",
          "--service", service,
          "--force-new-deployment",
          "--region", @region,
          "--cluster", @cluster
        ])
      end
    end
  end
end
