module Tenkai
  class RunHosts
    def initialize(cluster : String, certificate : String?, region : String, cmd : Array(String))
      @cluster     = cluster
      @certificate = certificate
      @region      = region
      @cmd         = cmd
    end

    alias Result = NamedTuple(
      status: Int32,
      output: String,
      arn:    String,
      ip:     String
    )

    def run
      spawned = 0
      channel = Channel(Result).new
      results = container_instances.map do |arn|
        spawned += 1
        spawn do
          ip = get_instance_ip(arn)
          run_command(arn, ip, channel)
        end
      end
      spawned.times do
        result = channel.receive
        report_result(result)
      end
    end

    def report_result(result : Result)
      str = "#{result[:ip]}> #{@cmd.join(" ")}"
      str = "[#{result[:status]}] #{str}" if result[:status] > 0
      if result[:status] == 0
        puts str.colorize.green
      else
        puts str.colorize.red
      end
      if result[:output].blank?
        puts "-----  NO OUTPUT  ------".colorize.yellow
      else
        puts "----- BEGIN OUTPUT -----".colorize.yellow
        puts result[:output]
        puts "-----  END OUTPUT  -----".colorize.yellow
      end
    end

    def container_instances
      response = JSON.parse Tenkai.cmd("aws", [
        "ecs", "list-container-instances",
        "--region", @region,
        "--cluster", @cluster
      ])
      response["containerInstanceArns"].as_a.map(&.as_s)
    end

    def run_command(arn, ip, channel)
      args  = [
        "-qt", "ec2-user@#{ip}",
        "-o", "StrictHostKeyChecking=no",
      ].concat(@cmd)
      unless @certificate.nil?
        args.unshift("-i", @certificate.to_s)
      end
      begin
        output = Tenkai.cmd("ssh", args)
        result = {
          status: 0,
          output: output,
          ip:     ip,
          arn:    arn
        }
      rescue e : Tenkai::FailedCommand
        result = {
          status: e.status,
          output: e.stderr,
          ip:     ip,
          arn:    arn
        }
      end
      channel.send(result)
    end

    def get_instance_ip(instance_arn : String)
      container_instances = JSON.parse Tenkai.cmd("aws", [
        "ecs", "describe-container-instances",
        "--region", @region,
        "--cluster", @cluster,
        "--container-instances", instance_arn
      ])
      instance_id = container_instances["containerInstances"][0]["ec2InstanceId"].as_s
      instance = JSON.parse Tenkai.cmd("aws", [
        "ec2", "describe-instances",
        "--region", @region,
        "--instance-ids", instance_id
      ])

      return instance["Reservations"][0]["Instances"][0]["PublicIpAddress"].as_s
    end
  end
end
