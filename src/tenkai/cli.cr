macro takes_region
  option "-r region",
    "--region region",
    type:    String,
    desc:    "The AWS region of the cluster",
    default: "[region]"
end
module Tenkai
  class CLI < Clim
    def self.parse_kv(args : Array(String)) : Hash(String, String?)
      env = {} of String => String?
      args.each do |arg|
        parts = arg.split("=", 2)
        if parts.size == 1
          puts "Invalid argument #{arg}"
          exit(1)
        else
          key, value = parts
          env[key] = value.empty?? nil : value
        end
      end
      return env
    end
    main_command do
      desc "ECS deployment management tool"
      usage "tenkai [sub_command] [arguments]"
      run do |options, arguments|
        puts Tenkai.config.rewrite(options.help)
      end

      # faux commands so they appear in the help
      sub_command "alias" do
        desc "Allows you to define a custom shorthand for a command."
        run do |options, arguments|
          puts Tenkai.config.rewrite(options.help)
        end
      end
      sub_command "aliases" do
        desc "Lists aliases."
        run do |options, arguments|
          puts Tenkai.config.rewrite(options.help)
        end
      end
      sub_command "unalias" do
        desc "Removes an alias."
        run do |options, arguments|
          puts Tenkai.config.rewrite(options.help)
        end
      end

      sub_command "configure" do
        desc "Set tenkai defaults. Available keys: region, maintenance-definition, console-command"
        usage "tenkai configure [key] [value]"
        run do |options, arguments|
          if arguments.size != 2
            puts Tenkai.config.rewrite(options.help)
          else
            Tenkai.config.update!(arguments[0], arguments[1])
            puts "Set #{arguments[0]} = #{arguments[1].inspect}"
          end
        end
      end
      sub_command "task" do
        desc "Task definition management (e.g env)"
        run do |options, arguments|
          puts Tenkai.config.rewrite(options.help)
        end
        sub_command "env" do
          desc "Manipulate the insecure env of task definitions"
          usage "tenkai env [get|set]"
          run do |options, arguments|
            puts Tenkai.config.rewrite(options.help)
          end

          sub_command "get" do
            desc "Print out the env of task definitions, optionally matching by keyword"
            usage "tenkai task env get [keyword1] [keyword2]"
            takes_region
            run do |options, arguments|
              Tenkai::Env.new(
                filters: arguments,
                region:  Tenkai.config.rewrite(options.region)
              ).print
            end
          end

          sub_command "set" do
            desc "Set environment variables in matching task definitions"
            usage "tenkai task env set [filter1] [filter2] [key1]=[value1?]"
            takes_region
            option "-d", "--dryrun", type: Bool, desc: "Don't actually set the env"
            run do |options, arguments|
              filters = [] of String
              keys = [] of String
              arguments.each do |argument|
                if argument.match(/=/)
                  keys.push(argument)
                else
                  filters.push(argument)
                end
              end
              if keys.empty?
                puts Tenkai.config.rewrite(options.help)
              else
                Tenkai::Env.new(
                  region:    Tenkai.config.rewrite(options.region),
                  filters:   filters
                ).set_envs(Tenkai::CLI.parse_kv(keys), dryrun: options.dryrun)
              end
            end
          end
        end
      end

      sub_command "env" do
        desc "Manipulate the secure env of task definitions"
        usage "tenkai env [get|set]"
        run do |options, arguments|
          puts Tenkai.config.rewrite(options.help)
        end

        sub_command "get" do
          desc "Print out environment variables under a namespace from SSM ParameterStore"
          usage "tenkai env get [namespace]"
          takes_region
          run do |options, arguments|
            if arguments.size != 1
              puts Tenkai.config.rewrite(options.help)
            else
              Tenkai::SecureEnv.new(
                namespace: arguments.first,
                region:    Tenkai.config.rewrite(options.region)
              ).print
            end
          end
        end
        sub_command "set" do
          desc "Set environment variables under a namespace from SSM ParameterStore"
          usage "tenkai env set [namespace] [key1]=[value1?]"
          takes_region
          run do |options, arguments|
            if arguments.size < 2
              puts Tenkai.config.rewrite(options.help)
            else
              Tenkai::SecureEnv.new(
                namespace: arguments.first,
                region:    Tenkai.config.rewrite(options.region)
              ).set_env(Tenkai::CLI.parse_kv(arguments[1..-1]))
            end
          end
        end
      end

      sub_command "restart" do
        desc "Restart all services in a cluster, optionally matching by keyword"
        usage "tenkai restart [cluster] [keyword1] [keyword2]"
        takes_region

        run do |options, arguments|
          if arguments.size == 0
            puts Tenkai.config.rewrite(options.help)
          else
            Tenkai::Restart.new(
              cluster: arguments[0],
              filters: arguments[1..-1],
              region:  Tenkai.config.rewrite(options.region)
            ).run
          end
        end
      end

      sub_command "maintenance" do
        desc "Manipulate maintenance mode - implemented by swapping task definitions"
        usage "tenkai maintenance [on|off] [cluster] [arguments]"
        run do |options, arguments|
          puts Tenkai.config.rewrite(options.help)
        end
        sub_command "on" do
          desc "Maintenance mode on"
          usage "tenkai maintenance on [cluster] [service] [arguments]"
          option "-t taskDefinition",
            "--task-definition=DEFINITION",
            type:    String,
            desc:    "The 'maintenance' task definition to use",
            default: "[maintenance-definition]"
          takes_region

          run do |options, arguments|
            if arguments.size < 2
              puts Tenkai.config.rewrite(options.help)
            else
              Tenkai::Maintenance.new(
                cluster:                arguments[0],
                service:                arguments[1],
                maintenance_definition: Tenkai.config.rewrite(options.task_definition),
                region:                 Tenkai.config.rewrite(options.region),
              ).on!
            end
          end
        end
        sub_command "off" do
          desc "Maintenance mode off"
          usage "tenkai maintenance off [cluster] [service] [arguments]"
          option "-t taskDefinition",
            "--task-definition=DEFINITION",
            type:    String,
            desc:    "The 'regular' task definition to use",
            default: "[CLUSTER]-[SERVICE]"
          takes_region

          run do |options, arguments|
            if arguments.size < 2
              puts Tenkai.config.rewrite(options.help)
            else
              task = if options.task_definition == "[CLUSTER]-[SERVICE]"
                       [arguments[0], arguments[1]].join("-")
                     else
                       options.task_definition
                     end

              Tenkai::Maintenance.new(
                cluster:    arguments[0],
                service:    arguments[1],
                definition: task,
                region:     Tenkai.config.rewrite(options.region),
              ).off!
            end
          end
        end
      end

      sub_command "console" do
        desc "Launch a console container and then SSH into it."
        usage "tenkai console [cluster] [arguments]"
        option "-t taskDefinition",
          "--task-definition=DEFINITION",
          type:    String,
          desc:    "The 'console' task definition to use",
          default: "[CLUSTER]-console"
        option "-i sshCert",
          "--identity sshCert",
          type: String,
          desc: "The ssh certificate to use"
        takes_region
        option "-c cmd",
          "--cmd cmd",
          type:    String,
          desc:    "The console command to run",
          default: "[console-command]"
	option "-u \"localFilesGlob\"",
	  "--upload \"localFilesGlob\"",
	  type:    String,
	  desc:    "Upload local files to the /app/upload directory on a docker instance. Then start session. E.g: -u \"./*.rb\""
        run do |options, arguments|
          if arguments.size == 0
            puts Tenkai.config.rewrite(options.help)
          else
            task = if options.task_definition == "[CLUSTER]-console"
                     [arguments[0], "console"].join("-")
                   else
                     options.task_definition
                   end

            Tenkai::Console.new(
              cluster:         arguments[0],
              task_definition: task,
              certificate:     options.identity,
              region:          Tenkai.config.rewrite(options.region),
              cmd:             Tenkai.config.rewrite(options.cmd),
	      copy_path:       options.upload
            ).run
          end
        end
      end
      sub_command "scale" do
        desc "Scale the concurrency on service(s) to an explicit count"
        usage "tenkai scale [cluster] [service]=[count]"
        takes_region
        run do |options, arguments|
          keys    = [] of String
          cluster = nil

          if arguments.size == 0
            puts Tenkai.config.rewrite(options.help)
            exit
          end

          arguments.each do |arg|
            if arg.includes?("=")
              keys.push(arg)
            elsif !cluster
              cluster = arg
            else
              puts Tenkai.config.rewrite(options.help)
              exit
            end
          end

          if !cluster || keys.empty?
            puts Tenkai.config.rewrite(options.help)
          else
            desired = {} of String => Tuple(UInt8, UInt8)
            Tenkai::CLI.parse_kv(keys).each do |k,v|
              if v
                parts = v.split(",", 2)
                if parts.size == 1
                  cast = v.to_u8
                  desired[k] = {cast, cast}
                else
                  min = parts[0].to_u8
                  max = parts[1].to_u8
                  desired[k] = {min, max}
                end
              else
                puts Tenkai.config.rewrite(options.help)
                exit(1)
              end
            rescue e : ArgumentError
              puts Tenkai.config.rewrite(options.help)
              exit(1)
            end
            Tenkai::Service.new(
              cluster: arguments.first,
              region:  Tenkai.config.rewrite(options.region)
            ).scale(desired)
          end
        end
      end
      sub_command "ps" do
        desc "Process (task) management routines"
        usage "tenkai ps [list|kill] [cluster] [arguments]"
        run do |options, arguments|
          puts Tenkai.config.rewrite(options.help)
        end

        sub_command "list" do
          desc "Print out the tasks running on the cluster."
          usage "tenkai ps list [cluster]"
          takes_region

          run do |options, arguments|
            if arguments.size == 0
              puts Tenkai.config.rewrite(options.help)
            else
              Tenkai::PS.new(
                cluster: arguments.first,
                region:  Tenkai.config.rewrite(options.region)
              ).print_tasks
            end
          end
        end

        sub_command "kill" do
          desc "Kill tasks on a cluster"
          usage "tenkai ps kill [cluster] [keyword1] [keyword2]"
          option "-a", "--all", type: Bool, desc: "Kill all tasks"
          option "-d", "--dryrun", type: Bool, desc: "Don't actually kill them"
          takes_region

          run do |options, arguments|
            case arguments.size
            when 0
              puts Tenkai.config.rewrite(options.help)
            when 1
              if options.all
                Tenkai::PS.new(
                  cluster: arguments.first,
                  region:  Tenkai.config.rewrite(options.region)
                ).kill_tasks_matching(%w[], dryrun: options.dryrun)
              else
                puts Tenkai.config.rewrite(options.help)
              end
            else
              Tenkai::PS.new(
                cluster: arguments.first,
                region:  Tenkai.config.rewrite(options.region)
              ).kill_tasks_matching(arguments[1..-1], dryrun: options.dryrun)
            end
          end
        end
      end
      sub_command "await" do
        desc "Launch a task definition and wait for it to finish."
        usage "tenkai await [cluster] [definition]"
        takes_region

        run do |options, arguments|
          if arguments.size < 2
            puts Tenkai.config.rewrite(options.help)
          else
            Tenkai::Await.new(
              cluster:         arguments[0],
              task_definition: arguments[1],
              region:          Tenkai.config.rewrite(options.region)
            ).run
          end
        end
      end
      sub_command "push" do
        desc "Push a docker image to ECR"
        usage "tenkai push [image]:[tag=latest]"
        takes_region
        run do |options, arguments|
          if arguments.size != 1
            puts Tenkai.config.rewrite(options.help)
          else
            Tenkai::Deploy.new(
              region: Tenkai.config.rewrite(options.region)
            ).push_image(arguments.first)
          end
        end
      end
      sub_command "versions" do
        desc "Print a list of available versions in ECR for a given image"
        usage "tenkai versions [image]"
        takes_region
        run do |options, arguments|
          if arguments.size != 1
            puts Tenkai.config.rewrite(options.help)
          else
            Tenkai::Deploy.new(
              region: Tenkai.config.rewrite(options.region)
            ).print_all_versions(arguments[0])
          end
        end
      end
      sub_command "deploy" do
        desc "Deploy a docker image to a cluster"
        usage "tenkai deploy [cluster] [image]:[tag=latest]"
        takes_region
        run do |options, arguments|
          if arguments.size != 2
            puts Tenkai.config.rewrite(options.help)
          else
            Tenkai::Deploy.new(
              region: Tenkai.config.rewrite(options.region)
            ).deploy(arguments[0], arguments[1])
          end
        end
      end
      sub_command "hosts" do
        desc "Host management utilities"
        usage "tenkai hosts [cmd] [cluster]"
        run do |options, arguments|
          puts Tenkai.config.rewrite(options.help)
        end
        sub_command "run" do
          desc "Run a command on all hosts"
          usage "tenkai hosts run [cluster] [cmd...]"
          option "-i sshCert",
            "--identity sshCert",
            type: String,
            desc: "The ssh certificate to use"
          takes_region
          run do |options, arguments|
            if arguments.size < 2
              puts Tenkai.config.rewrite(options.help)
            else
              Tenkai::RunHosts.new(
                cluster:     arguments[0],
                region:      Tenkai.config.rewrite(options.region),
                certificate: options.identity,
                cmd:         arguments[1..-1]
              ).run
            end
          end
        end
        sub_command "test" do
          desc "Test all hosts are reachable"
          usage "tenkai hosts test [cluster]"
          option "-i sshCert",
            "--identity sshCert",
            type: String,
            desc: "The ssh certificate to use"
          takes_region
          run do |options, arguments|
            if arguments.size != 1
              puts Tenkai.config.rewrite(options.help)
            else
              Tenkai::TestHosts.new(
                cluster:     arguments[0],
                region:      Tenkai.config.rewrite(options.region),
                certificate: options.identity
              ).run
            end
          end
        end
      end
    end
  end
end
