module Tenkai
  class TestHosts
    def initialize(cluster : String, certificate : String?, region : String)
      @cluster     = cluster
      @certificate = certificate
      @region      = region
    end

    def run
      spawned = 0
      channel = Channel(String).new
      results = container_instances.map do |arn|
        spawned += 1
        spawn do
          ip          = get_instance_ip(arn)
          can_connect = test_ssh_connection(ip)
          channel.send report(arn, ip, can_connect)
        end
      end
      puts("  %15s %s" % ["IP Address", "ARN"])
      spawned.times do
        puts channel.receive
      end
    end

    def report(arn : String, ip : String, can_connect : Bool)
      # It just outputs cluster name and container GUID
      output = [ip, arn.split("/", 2).last]
      if can_connect
        output.unshift "✔"
      else
        output.unshift "✘"
      end
      str = "%s %15s %s" % output
      if can_connect
        str.colorize.green.to_s
      else
        str.colorize.red.to_s
      end
    end

    def container_instances
      response = JSON.parse Tenkai.cmd("aws", [
        "ecs", "list-container-instances",
        "--region", @region,
        "--cluster", @cluster
      ])
      response["containerInstanceArns"].as_a.map(&.as_s)
    end

    def test_ssh_connection(ip)
      args  = [
        "-qt", "ec2-user@#{ip}",
        "-o", "StrictHostKeyChecking=no",
        "echo Y"
      ]
      unless @certificate.nil?
        args.unshift("-i", @certificate.to_s)
      end
      begin
        Tenkai.cmd("ssh", args)
      rescue e : Tenkai::FailedCommand
        return false
      end
      return true
    end

    def get_instance_ip(instance_arn : String)
      container_instances = JSON.parse Tenkai.cmd("aws", [
        "ecs", "describe-container-instances",
        "--region", @region,
        "--cluster", @cluster,
        "--container-instances", instance_arn
      ])
      instance_id = container_instances["containerInstances"][0]["ec2InstanceId"].as_s
      instance = JSON.parse Tenkai.cmd("aws", [
        "ec2", "describe-instances",
        "--region", @region,
        "--instance-ids", instance_id
      ])

      return instance["Reservations"][0]["Instances"][0]["PublicIpAddress"].as_s
    end
  end
end
