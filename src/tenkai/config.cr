module Tenkai
  class Config
    DEFAULT_CONFIG = {
      "region"                 => "ap-southeast-2",
      "maintenance-definition" => "maintenance-page",
      "console-command"        => "bash",
      "aliases"                => {} of String => Array(String)
    }

    @region : String
    @maintenance_definition : String
    @console_command : String
    @aliases : Hash(String, Array(String))

    def initialize
      write_default_config!

      doc                     = JSON.parse(File.open(config_file_location)).as_h
      @region                 = or_default(doc, "region")
      @maintenance_definition = or_default(doc, "maintenance-definition")
      @console_command        = or_default(doc, "console-command")
      @aliases                = parse_aliases(doc)
    end

    def rewrite(str)
      str = str.dup
      DEFAULT_CONFIG.keys.each do |key|
        next if key == "aliases"
        str = str.gsub("[#{key}]", get(key))
      end
      str
    end

    def has_alias?(key : String)
      @aliases.has_key?(key)
    end

    def get_alias(key : String)
      @aliases[key].dup
    end

    def add_alias(args : Array(String))
      unless args.size > 1
        puts "Usage: tenkai alias [name] command..."
        exit(1)
      end
      @aliases[args[0]] = args[1..-1]
      write_config!
      puts "Added alias #{args[0]}"
    end

    def remove_alias(args : Array(String))
      unless args.size == 1
        puts "Usage: tenkai unalias [name]"
        return
      end
      if @aliases.has_key?(args[0])
        @aliases.delete(args[0])
        write_config!
        puts "Removed alias #{args[0]}"
      else
        puts "Alias #{args[0]} not found"
      end
    end

    def list_aliases
      if @aliases.empty?
        puts "No aliases defined."
        return
      end
      puts "Aliases:"
      puts
      max_key_length = @aliases.keys.map(&.size).max
      @aliases.each do |key, value|
        print(" "*(max_key_length - key.size))
        print key
        print " -> "
        puts value.join(" ")
      end
    end

    def get(key : String) : String
      case key
      when "region"
        @region
      when "maintenance-definition"
        @maintenance_definition
      when "console-command"
        @console_command
      else
        ""
      end
    end

    def parse_aliases(doc)
      if doc.has_key?("aliases")
        aliases = {} of String => Array(String)
        doc["aliases"].as_h.each do |name, args|
          aliases[name] = args.as_a.map(&.as_s)
        end
        return aliases
      else
        return {} of String => Array(String)
      end
    end

    def or_default(doc, key) : String
      if doc.has_key?(key)
        doc[key].as_s
      else
        DEFAULT_CONFIG[key].to_s
      end
    end

    def update!(key, value)
      case key
      when "region"
        @region = value
      when "maintenance-definition"
        @maintenance_definition = value
      when "console-command"
        @console_command = value
      else
        puts "Invalid key #{key}."
        exit(1)
      end
      write_config!
    end

    def write_default_config!
      unless File.exists?(config_file_location)
        File.open(config_file_location, "w") do |f|
          f.puts DEFAULT_CONFIG.to_pretty_json
        end
      end
    end

    def write_config!
      doc = {
        "region"                 => @region,
        "maintenance-definition" => @maintenance_definition,
        "console-command"        => @console_command,
        "aliases"                => @aliases
      }
      File.open(config_file_location, "w") do |f|
        f.puts doc.to_pretty_json
      end
    end

    def config_file_location
      File.expand_path("~/.tenkai.conf", home: true)
    end
  end
end
