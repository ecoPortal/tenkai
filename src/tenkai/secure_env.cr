module Tenkai
  class SecureEnv
    def initialize(region : String, namespace : String)
      @region    = region
      if namespace[0] != "/"
        @namespace = "/#{namespace}"
      else
        @namespace = namespace
      end
    end
    def print
      print_env(get_env)
    end
    def print_env(env : Hash(String, String?))
      if env.empty?
        puts "No environment variables set!"
        return
      end
      max_key_size = env.keys.sort_by {|x| x.size}.last.size
      env.keys.sort.each do |key|
        print key
        print ":"
        print " "*(max_key_size - key.size + 1)
        puts env[key]
      end
    end
    def get_env : Hash(String, String)
      response = Tenkai.cmd("aws", [
        "ssm", "get-parameters-by-path",
        "--path", @namespace,
        "--region", @region,
        "--with-decryption"
      ])
      JSON.parse(response)["Parameters"].as_a.map do |parameter|
        [
          parameter["Name"].as_s.split("/").last,
          parameter["Value"].as_s
        ]
      end.to_h
    end
    def set_env(env : Hash(String, String | Nil))
      env.each do |key, value|
        if value.nil?
          delete_parameter(key)
        else
          put_parameter(key, value)
        end
      end
      puts "Updated secure env:"
      puts "="*80
      print_env(env)
    end
    def delete_parameter(name : String)
      Tenkai.cmd("aws", [
        "ssm", "delete-parameter",
        "--name", namespaced(name),
        "--region", @region,
      ])
    rescue Tenkai::FailedCommand
      puts "Parameter #{name} not found, skipping..."
    end
    def put_parameter(name : String, value : String)
      Tenkai.cmd("aws", [
        "ssm", "put-parameter",
        "--name", namespaced(name),
        "--value", value,
        "--type", "SecureString",
        "--region", @region,
        "--overwrite"
      ])
    end
    def namespaced(name : String)
      [@namespace, name].join("/")
    end
  end
end
