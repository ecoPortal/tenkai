build: mkbin
	crystal build -o bin/tenkai src/tenkai.cr
build-release: mkbin
	crystal build -o bin/tenkai src/tenkai.cr --release --static
mkbin:
	mkdir -p bin
clean:
	rm bin/tenkai
