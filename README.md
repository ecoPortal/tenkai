# Tenkai

Simple tool to help manage AWS ECS/ECR, with some limited support for parameter store.

## Building

Tenkai is written in [crystal](https://crystal-lang.org) and needs both
`crystal` and `shards` installed in order to build.

```sh
shards install
make
```

## Usage

```sh
tenkai
```

### Configuration

There are several default values that you can configure. These are:

- `region` - what AWS region to target (default: ap-southeast-2)
- `maintenance-definition` - what task definition to use with `tenkai maintenance on`
- `console-command` - what command to run with `tenkai console`

You can configure these with `tenkai configure`. Settings are stored in
`~/.tenkai.conf` in JSON format.

### Console

`tenkai console` assumes that you have:

- A task definition which is usable as a console
- The ability to SSH into all EC2 instances in your cluster
- Spare capacity in your cluster to launch the task definition

It will launch a task using the definition passed, wait for it to start running
and then connect to the container.

### Environment manipulation

`tenkai task env get` lets you print out the insecure env stored on the task
definitions, optionally filtered by keyword. To print out the env of past
versions of task definitions, include the version in your keyword(s).

`tenkai task env set` lets you set or remove env variables stored in the task
definitions, optionally filtered by keyword. e.g:

- `tenkai task env set maintenance FOO=bar` - set `FOO=bar` on task definitions
  which contain "maintenance" in their family name
- `tenkai task env set maintenance FOO=` - unset `FOO` on task definitions
  which contain "maintenance" in their family name

`tenkai env get` lets you print out parameters from Parameter Store matching
a namespace. If you have a parameter `/foo/BAR=encrypted("Baz")`,
`tenkai env get foo` will print out `BAR: Baz`.

`tenkai env set` lets you add namespaced parameters to Parameter Store. If
you leave the value part blank, it will delete the parameter. e.g:

- `tenkai env set foo FOO=bar` - set `/foo/FOO=encrypted("bar")`
- `tenkai env set foo FOO=` - unset `/foo/FOO`
- `tenkai env set foo FOO="something with spaces"` - more complicated parameter

### Maintenance mode

`tenkai maintenance` assumes that you have:

- A task definition which you want to take over during periods of
  maintenance (note that the container name in this task definition must
  match the container name in the service you're replacing)
- Spare capacity in your cluster to launch the task definition

It will update your service to use either the maintenance task definition
or your regular task definition depending on if you call on or off.

### Process management

`tenkai ps list` will print out the tasks currently running in your cluster with
their definition, task ID and who started them.

`tenkai ps kill` will allow you to manually kill processes running in your
cluster.

### One-off tasks

`tenkai await` lets you launch a task in a cluster and then wait for it to
complete. For example, you can define a task in ECS to push your assets to a
CDN and then use `tenkai await` to run it as a part of your deployment workflow.

### Deployments

`tenkai restart` lets you restart the services in a cluster, optionally filtered
by keyword, by forcing a new service deployment.

### Service management

`tenkai scale` lets you set the concurrency for services. It will either set the
desired count if there is no auto-scaling configured, or it will update the
scalable target's min/max values. e.g:

- `tenkai scale [cluster] web=2,4` would configure web service auto-scaling to min 2 max 4
- `tenkai scale [cluster] worker=5` would either:
  - Configure the worker service to desired count 5 if no autoscaling is set; or
  - Configure the worker scaling target to min=5 max=5 if it exists

### Deployments

`tenkai push` lets you push an image to ECR. You can either specify a tag, or it
will automatically append the `:latest` tag.

`tenkai deploy` will add a version timestamp and the `:active` tag to the image
you specify. It will then find all services in your cluster which are connected
to `imagename:active` and restart them. During this process, if you specify a
local image it will be pushed as with `tenkai push`. However, you can also
specify a full path to an existing tagged image on ECR in order to perform a
rollback to a previous active version by tagging it `active`.

`tenkai versions` lets you list all the versions in ECR for a given image. The
currently active tag will have an * next to it.

### Host management

`tenkai hosts test` lets you list out all the EC2 instances connected to a given
ECS cluster and run a basic SSH connection test to each. Can be used to ensure
that all hosts are responding and that the prerequisites for tenkai are working.

`tenkai hosts run` lets you run a command on every EC2 instance in a given ECS
cluster. It can be used to e.g install updates (`sudo yum update -y`) or perform
other common maintenance tasks like updating `.ssh/authorized_keys`. For more
complicated commands that require options and so on, make sure to quote your
command as a single argument so that it doesn't get eaten by local option
parsing.

### Aliases

As the types of commands you run with tenkai are usually pretty similar over
time, a basic facility to define your own command aliases is included. This
allows you to save your most typical command strings for easier use later.

You can add an alias with `tenkai alias`, remove one with `tenkai unalias`,
and list all of your aliases with `tenkai aliases`.
